﻿using Core.Api.Security.ViewModel;
using Core.Domain.Interfaces.IApps;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Core.Api.Security.Controllers
{
	[RoutePrefix("user")]
    public class UserController : ApiController
    {
		private IUserApp _app;
		public UserController(IUserApp app)
		{
			_app = app;
		}

		[HttpPost]
		[Route("register")]
		public async Task<IHttpActionResult> Register(RegisterBindingModel model)
		{
			var _value = await _app.Register(model);
			return Ok(_value);
		}
	}
}
