﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;
using Core.Infa.CrossCutting.IoC;
using System.Web.Routing;
using Core.Infra.CrossCutting.Mapper;

namespace Core.Api.Security
{
    public class WebApiApplication : System.Web.HttpApplication
    {
        protected void Application_Start()
        {
            GlobalConfiguration.Configure(WebApiConfig.Register);
			MapperConfig.RegisterMappings();

		}
    }
}
