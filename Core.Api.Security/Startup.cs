﻿using Core.Api.Security.App_Start;
using Microsoft.Owin;
using Microsoft.Owin.Security.OAuth;
using Owin;
using SimpleInjector.Integration.WebApi;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

[assembly: OwinStartup(typeof(Core.Api.Security.Startup))]
namespace Core.Api.Security
{
	public class Startup
	{
		public void Configuration(IAppBuilder app)
		{
			var container = DIConfig.InjectorInitialize();
			HttpConfiguration config = new HttpConfiguration();
			config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

			ConfigureOAuth(app);
			WebApiConfig.Register(config);
			app.UseCors(Microsoft.Owin.Cors.CorsOptions.AllowAll);
			app.UseWebApi(config);
		}
		private void ConfigureOAuth(IAppBuilder app)
		{
			//Token Consumption
			app.UseOAuthBearerAuthentication(new OAuthBearerAuthenticationOptions
			{
			});
		}
	}
}