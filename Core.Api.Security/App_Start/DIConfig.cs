﻿using Core.Infa.CrossCutting.IoC;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;
using SimpleInjector.Integration.WebApi;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace Core.Api.Security.App_Start
{
	public static class DIConfig
	{
		public static Container InjectorInitialize()
		{
			var container = new Container();
			container.Options.DefaultScopedLifestyle = new AsyncScopedLifestyle();
			InitializeContainer(container);

			container.RegisterWebApiControllers(GlobalConfiguration.Configuration);
			container.Verify();
			return container;
		}

		private static void InitializeContainer(Container container)
		{
			DI.Register(container);
		}
	}
}