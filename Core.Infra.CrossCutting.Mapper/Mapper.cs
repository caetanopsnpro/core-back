﻿namespace Core.Infra.CrossCutting.Mapper
{
	public class Mapper<S, T> 
		where S: class
		where T: class
	{
		public Mapper() { }
		public T MapToDomain(dynamic model)
		{
			return AutoMapper.Mapper.Map<S, T>(model);
		}
	}
}
