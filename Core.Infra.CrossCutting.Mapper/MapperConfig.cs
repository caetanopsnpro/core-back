﻿using AutoMapper;
using Core.Api.Security.ViewModel;
using Core.Domain.Entities;

namespace Core.Infra.CrossCutting.Mapper
{
	public class MapperConfig
	{
		public static void RegisterMappings()
		{
			AutoMapper.Mapper.Initialize(x =>
			{
				x.CreateMap<RegisterBindingModel, User>();
			});
		}
	}
}
