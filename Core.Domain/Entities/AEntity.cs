﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

namespace Core.Domain.Entities
{
	public abstract class AEntity
	{
		public Guid Id { get; set; }
		public DateTime DtInclusao { get; set; }
		public DateTime? DtAlteracao { get; set; }
	}
}
