﻿using Core.Domain.BusinessRules;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Entities
{
	public class User: IdentityUser
	{
		private UserValidation _validationRules;
		public User() {
			this._validationRules = new UserValidation(this);
		}
		//public User(string email, string userName) 
		//{
		//	this.Email = email;
		//	this.UserName = userName;
		//	this._validationRules = new UserValidation(this);
		//}

		public UserValidation ValidationRules { get { return _validationRules; } protected set { } }

	}
}
