﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Helpers
{
	public static class SystemExtensions
	{
		#region String
		public static bool IsNullOrEmpty(this string str) {
			return string.IsNullOrEmpty(str);
		}
		#endregion
	}
}
