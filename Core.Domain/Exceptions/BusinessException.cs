﻿using System;
using System.Collections.Generic;
using System.Linq;

namespace Core.Domain.Exceptions
{
	public class BusinessException: Exception
	{
		private List<string> _listOfErrors;
		public BusinessException() : base() {
			_listOfErrors = new List<string>();
		}
		public void AddError(string errorCode) => _listOfErrors.Add(errorCode);
		public string[] GetErrors() => _listOfErrors.ToArray();
		public bool HasError() => _listOfErrors.Any();
	}
}
