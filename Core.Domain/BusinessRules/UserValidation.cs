﻿using Core.Domain.Entities;
using Core.Domain.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.BusinessRules
{
	public class UserValidation
	{
		private User _user;
		public UserValidation(User user)
		{
			_user = user;
		}

		public void ValidateForRegister() {
			var ex = new BusinessException();
			if (string.IsNullOrEmpty(this._user.Email))
				ex.AddError("E-mail não informado.");
			if (string.IsNullOrEmpty(this._user.UserName))
				ex.AddError("Username não informado.");

			if (ex.HasError())
				throw ex;
		}
	}
}
