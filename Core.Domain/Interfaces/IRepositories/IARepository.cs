﻿using System.Collections.Generic;
using System.Linq;
using Core.Domain.Entities;

namespace Core.Domain.Interfaces.IRepositories
{
	public interface IARepository<TEntity> where TEntity : AEntity
	{
		void Add(TEntity obj);
		void AddAll(IEnumerable<TEntity> obj);
		void DeleteAll(IEnumerable<TEntity> obj);
		void Delete(TEntity obj);
		void Delete(int id);

		TEntity Get(int id);

		TEntity First();
		IQueryable<TEntity> Get();
		void Update(TEntity obj);

		void AddOrUpdate(TEntity obj);
	}

}