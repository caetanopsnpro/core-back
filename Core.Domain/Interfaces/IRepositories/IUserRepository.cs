﻿using Core.Domain.Entities;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Interfaces.IRepositories
{
	public interface IUserRepository
	{
		Task<User> FindByEmail(string email);
		Task<IdentityResult> Create(User user, string password);
	}
}
