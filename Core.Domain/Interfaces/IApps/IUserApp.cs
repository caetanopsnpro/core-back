﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Interfaces.IApps
{
	public interface IUserApp
	{
		Task<bool> Register(dynamic model);
	}
}
