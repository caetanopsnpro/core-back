﻿using Core.Domain.Entities;
using Core.Domain.Interfaces.IRepositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Domain.Interfaces.IUnitOfWork
{
	public interface IUnityOfWork<TEntity> 
		where TEntity : AEntity
	{
		void Save();
		void Dispose();
	}
}
