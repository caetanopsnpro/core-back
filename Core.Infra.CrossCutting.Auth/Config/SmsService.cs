﻿using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infra.CrossCutting.Auth.Config
{
	public class SmsService : IIdentityMessageService
	{
		//TODO: Habilitar / Implementar o envio de sms
		public Task SendAsync(IdentityMessage message)
		{
			// Utilizando TWILIO como SMS Provider.
			// https://www.twilio.com/docs/quickstart/csharp/sms/sending-via-rest

			//const string accountSid = "SEU ID";
			//const string authToken = "SEU TOKEN";

			//var client = new TwilioRestClient(accountSid, authToken);

			//client.SendMessage("Seu Telefone", message.Destination, message.Body);

			return Task.FromResult(0);
		}
	}
}
