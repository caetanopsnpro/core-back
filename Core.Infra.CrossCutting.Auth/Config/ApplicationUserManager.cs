﻿using Core.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infra.CrossCutting.Auth.Config
{
	public class ApplicationUserManager : UserManager<User>
	{
		public ApplicationUserManager(IUserStore<User> store)
			: base(store)
		{
			// Configurando validator para nome de usuario
			UserValidator = new UserValidator<User>(this)
			{
				AllowOnlyAlphanumericUserNames = false,
				RequireUniqueEmail = true
			};

			// Logica de validação e complexidade de senha
			PasswordValidator = new PasswordValidator
			{
				RequiredLength = 6,
				RequireNonLetterOrDigit = false,
				RequireDigit = false,
				RequireLowercase = false,
				RequireUppercase = false,
			};

			// Configuração de Lockout
			UserLockoutEnabledByDefault = true;
			DefaultAccountLockoutTimeSpan = TimeSpan.FromMinutes(5);
			MaxFailedAccessAttemptsBeforeLockout = 5;

			// Providers de Two Factor Autentication
			RegisterTwoFactorProvider("Código via SMS", new PhoneNumberTokenProvider<User>
			{
				MessageFormat = "Seu código de segurança é: {0}"
			});

			RegisterTwoFactorProvider("Código via E-mail", new EmailTokenProvider<User>
			{
				Subject = "Código de Segurança",
				BodyFormat = "Seu código de segurança é: {0}"
			});

			// Definindo a classe de serviço de e-mail
			EmailService = new EmailService();

			// Definindo a classe de serviço de SMS
			SmsService = new SmsService();

			var provider = new MachineKeyProtectionProvider();
			this.UserTokenProvider = new DataProtectorTokenProvider<User>(
				provider.Create("CiLN&@17*209"));

		}
	}

}
