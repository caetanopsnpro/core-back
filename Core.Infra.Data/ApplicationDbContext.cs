﻿using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using Core.Domain.Entities;
using Core.Infra.Data.EntityTypeConfigurations;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Reflection;
using Core.Infra.Data.Mapping;
using System.Linq;

namespace Core.Infra.Data
{
	public class ApplicationDbContext : IdentityDbContext<User>
	{
		public ApplicationDbContext() : base("FR_CONNECTION")
		{
			Configuration.LazyLoadingEnabled = false;
		}

		public static ApplicationDbContext Create()
		{
			var ensureDLLIsCopied = System.Data.Entity.SqlServer.SqlProviderServices.Instance;
			return new ApplicationDbContext();
		}

		protected override void OnModelCreating(DbModelBuilder modelBuilder)
		{
			modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
			modelBuilder.Conventions.Remove<OneToManyCascadeDeleteConvention>();
			modelBuilder.Conventions.Remove<ManyToManyCascadeDeleteConvention>();

			var mappings = (from x in Assembly.GetAssembly(typeof(AMapping<>)).GetTypes()
					where x.IsClass
					&& !x.IsAbstract
					&& x.BaseType.IsGenericType
					&& x.BaseType.GetGenericTypeDefinition() == typeof(AMapping<>)
					select x).ToList();

			modelBuilder.Configurations.Add(new UserMapping());
			mappings.ForEach(x => modelBuilder.Configurations.Add((dynamic)Activator.CreateInstance(x)));
			base.OnModelCreating(modelBuilder);
		}
	}
}
