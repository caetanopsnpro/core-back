﻿using Core.Domain.Entities;
using Core.Domain.Interfaces.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infra.Data.UnityOfWork
{
	public class UnityOfWork<T>: IUnityOfWork<T> 
		where T : AEntity
	{
		public void Dispose()
		{
			GC.SuppressFinalize(this);
		}

		public void Save() { }
	}
}
