﻿using Core.Domain.Entities;
using Core.Domain.Interfaces.IRepositories;
using Core.Infra.CrossCutting.Auth;
using Core.Infra.CrossCutting.Auth.Config;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infra.Data.Repositories
{
	public class UserRepository: IUserRepository
	{
		protected readonly ApplicationUserManager _manager;

		public UserRepository(ApplicationUserManager manager)
		{
			_manager = manager;
		}
		public async Task<User> FindByEmail(string email) => await _manager.FindByEmailAsync(email);
		public async Task<IdentityResult> Create(User user, string password) => await _manager.CreateAsync(user, password);
	}
}
