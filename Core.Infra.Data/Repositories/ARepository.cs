﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Validation;
using System.Linq;
using Core.Domain.Entities;
using Core.Domain.Interfaces.IRepositories;

namespace Core.Infra.Data.Repositories
{
	public abstract class ARepository<TEntity> : IARepository<TEntity> 
		where TEntity : AEntity
	{
		protected readonly ApplicationDbContext _context;

		public ARepository(ApplicationDbContext context)
		{
			_context = context;
		}

		private DbSet<TEntity> Entity { get { return _context.Set<TEntity>(); } }

		public void Add(TEntity obj)
		{
			obj.DtInclusao = DateTime.Now;
			Entity.Add(obj);
		}

		public void AddAll(IEnumerable<TEntity> obj)
		{
			foreach (var entity in obj)
				Add(entity);
		}

		public void DeleteAll(IEnumerable<TEntity> obj)
		{
			foreach (var entity in obj)
				Delete(entity);
		}

		public void Delete(TEntity obj)
		{
			Entity.Remove(obj);
		}

		public void Delete(int id)
		{
			Entity.Remove(Get(id));
		}

		public TEntity Get(int id)
		{
			return Entity.Find(id);
		}

		public TEntity First()
		{
			return Entity.FirstOrDefault();
		}

		public IQueryable<TEntity> Get()
		{
			return Entity;
		}

		public void Update(TEntity obj)
		{
			obj.DtAlteracao = DateTime.Now;
			_context.Entry(obj).State = EntityState.Modified;
		}

		public void AddOrUpdate(TEntity obj)
		{
			if (obj.Id != Guid.Empty)
				Update(obj);
			else
				Add(obj);
		}
	}
}