using System.Linq;
using Core.Domain.Entities;
using Core.Domain.Interfaces.IRepositories;
using Core.Domain.ValueObject;
using Core.Domain.Interfaces.IUnitOfWork;

namespace Core.Infra.Data.Repositories
{
	public class ExampleRepository : ARepository<Example>, IExampleRepository
	{
		public ExampleRepository(ApplicationDbContext context)
			: base(context) { }
	}
}