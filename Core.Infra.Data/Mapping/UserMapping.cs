﻿using System.Data.Entity.ModelConfiguration;
using Core.Domain.Entities;

namespace Core.Infra.Data.EntityTypeConfigurations
{
	public class UserMapping : EntityTypeConfiguration<User>
	{
		public UserMapping()
		{
			ToTable("AspNetUsers", "dbo");
			Ignore(x => x.ValidationRules);
		}
	}
}
