﻿using Core.Domain.Entities;
using Core.Domain.Interfaces.IMapping;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infra.Data.Mapping
{
	public abstract class AMapping<TEntity>: EntityTypeConfiguration<TEntity>, IAMapping<TEntity> where TEntity: AEntity
	{
		public AMapping(string schema, string tableName)
		{
			ToTable(tableName, schema);
			HasKey(x => x.Id);
			Property(u => u.Id)
				.IsRequired()
				.HasDatabaseGeneratedOption(DatabaseGeneratedOption.Identity);
		}
	}
}
