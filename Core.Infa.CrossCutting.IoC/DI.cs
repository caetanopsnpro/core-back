﻿using Core.App;
using Core.Domain.Entities;
using Core.Domain.Interfaces.IApps;
using Core.Domain.Interfaces.IRepositories;
using Core.Domain.Interfaces.IUnitOfWork;
using Core.Infra.CrossCutting.Auth;
using Core.Infra.CrossCutting.Auth.Config;
using Core.Infra.Data;
using Core.Infra.Data.Repositories;
using Core.Infra.Data.UnityOfWork;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleInjector;
using SimpleInjector.Diagnostics;
using SimpleInjector.Lifestyles;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Core.Infa.CrossCutting.IoC
{
	public static class DI
	{
		public static void Register(Container container)
		{
			var repositoryAssembly = typeof(ARepository<>).Assembly;
			var appAssembly = typeof(AApp<>).Assembly;

			var repositorys = (from x in Assembly.GetAssembly(typeof(ARepository<>)).GetTypes()
				where x.IsClass 
				&& !x.IsAbstract 
				&& x.BaseType.IsGenericType 
				&& x.BaseType.GetGenericTypeDefinition() == typeof(ARepository<>) 
				select new { 
					Entity = x, 
					Interface = x.GetInterfaces().FirstOrDefault(q => q.Name.Contains(nameof(x))) 
				}).ToList();

			var apps = (from x in Assembly.GetAssembly(typeof(AApp<>)).GetTypes()
				where x.IsClass
				&& !x.IsAbstract
				&& x.BaseType.IsGenericType
				&& x.BaseType.GetGenericTypeDefinition() == typeof(AApp<>)
				select new { 
					Entity = x, 
					Interface = x.GetInterfaces().FirstOrDefault(q => q.Name.Contains(nameof(x))) 
				}).ToList();

			container.Register<ApplicationDbContext>(Lifestyle.Scoped);
			container.Register<IUserStore<User>>(() => new UserStore<User>(container.GetInstance<ApplicationDbContext>()), Lifestyle.Scoped);
			container.Register<ApplicationUserManager>(Lifestyle.Scoped);

			//User Identity
			container.Register(typeof(IUserRepository), typeof(UserRepository), Lifestyle.Scoped);
			container.Register(typeof(IUserApp), typeof(UserApp), Lifestyle.Scoped);

			//Generic Pattern
			container.Register(typeof(IUnityOfWork<>), typeof(UnityOfWork<>), Lifestyle.Scoped);
			container.Register(typeof(IARepository<>), repositorys.Select(x => x.Entity), Lifestyle.Scoped);
			container.Register(typeof(IAApp<>), apps.Select(x => x.Entity), Lifestyle.Scoped);

			repositorys.ForEach(x => container.Register(x.Interface, x.Entity, Lifestyle.Scoped));
			apps.ForEach(x => container.Register(x.Interface, x.Entity, Lifestyle.Scoped));
		}
	}
}
