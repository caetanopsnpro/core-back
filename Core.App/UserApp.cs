﻿using Core.Api.Security.ViewModel;
using Core.Domain.Entities;
using Core.Domain.Exceptions;
using Core.Domain.Interfaces.IApps;
using Core.Domain.Interfaces.IRepositories;
using Core.Infra.CrossCutting.Mapper;
using System;
using System.Threading.Tasks;

namespace Core.App
{
	public class UserApp : IUserApp
	{
		protected IUserRepository _repository;
		public UserApp(IUserRepository repository)
		{
			_repository = repository;
		}

		public async Task<bool> Register(dynamic model)
		{
			try
			{
				//var user = new User(
				// 	email: model.Email.ToString(),
				//	userName: model.Email.ToString()
				//);
				var user = new Mapper<RegisterBindingModel, User>().MapToDomain(model);
				user.ValidationRules.ValidateForRegister();

				var result = await _repository.Create(user, model.Password.ToString());
				return false;
			}
			catch (BusinessException e)
			{
				throw e;
			}
			catch (Exception e)
			{
				throw e;
			}
		}
	}
}
