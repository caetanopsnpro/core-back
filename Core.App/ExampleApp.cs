﻿using System;
using Core.Domain.Entities;
using Core.Domain.Interfaces.IApps;
using Core.Domain.Interfaces.IRepositories;
using Core.Domain.ValueObject;
using Core.Domain.Interfaces.IUnitOfWork;

namespace Core.App
{
	public class ExampleApp : AApp<Example>, IExampleApp
	{
		public ExampleApp(IARepository<Example> repository, IUnityOfWork<Example> unityOfWork)
			:base(repository, unityOfWork){ }
			
	}
}
