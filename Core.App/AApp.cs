﻿using Core.Domain.Entities;
using Core.Domain.Interfaces.IApps;
using Core.Domain.Interfaces.IRepositories;
using Core.Domain.Interfaces.IUnitOfWork;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Core.App
{
	public abstract class AApp<TEntity> : IAApp<TEntity> 
		where TEntity : AEntity
	{
		protected IARepository<TEntity> _repository;
		protected IUnityOfWork<TEntity> _unityOfWork;
		protected AApp(IARepository<TEntity> repository, IUnityOfWork<TEntity> unityOfWork) {
			_repository = repository;
			_unityOfWork = unityOfWork;
		}
	}
}
